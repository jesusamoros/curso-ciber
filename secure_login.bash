#!/bin/bash

echo "Bienvenido, este programa usa BASH"
echo "Pulsa 1 para crear un nuevo usuario"
echo "Pulsa 2 para descifrar los datos de los usuarios"
read variable


if [[ $variable == 1 ]]
then
    echo  "Introduce su email"
    read email
    echo "introduce su pass"
    read pass
    echo $email >> base_datos.txt
    hash=$(echo $pass |  sha256sum)
    echo $hash >> base_datos.txt
    echo "Cifrando base de datos"
    openssl enc -aes256 -salt -pbkdf2 -in base_datos.txt -out base_datos.enc
    echo "eliminando base de datos sin cifrar base_datos.txt"
    rm base_datos.txt && echo "base de datos eliminada"
    echo "Base de datos cifrada base_datos.enc"
   exit 0
fi
if [[ $variable == 2 ]]
then
    echo  "Deseas descifrar la base de datos"
    openssl enc -aes256 -pbkdf2 -d -in base_datos.enc -out base_datos.txt
    echo "base de datos descifrada base_datos.txt"
